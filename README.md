# shellcheck

[ShellCheck](https://github.com/koalaman/shellcheck) is a shell script static
analysis tool.

## Using in the Pipeline

To enable shellcheck in your project's pipeline add `shellcheck` to the
`ENABLE_JOBS` variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "shellcheck"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the [ShellCheck extension](https://marketplace.visualstudio.com/items?itemName=timonwong.shellcheck)
for Visual Studio Code.

## Using Locally in Your Development Environment

shellcheck can be run locally in a shell script in a linux-based bash shell.
[`shellcheck.sh`](shellcheck.sh) in this project is an example that you can copy
to your project or paste into your own shell script.

## Configuration

You can configure shellcheck to [ignore certain errors](https://github.com/koalaman/shellcheck/wiki/Ignore).

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
